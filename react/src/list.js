import React, { useState,useEffect } from "react";

import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import "./login.css";
import axios from 'axios';
import Form from "react-bootstrap/Form";
import { useNavigate } from "react-router-dom";
export default function List() {
	const [list,setList] = useState([])
	const [search, setsearch] = useState("");
	let navigate = useNavigate();
	useEffect(()=>{
		
		axios.post(`http://localhost:9999/user/search`,"" )
			  .then(res => {
				console.log(res);
				console.log(res.data);
				if(res.data.success == true)
				{
					setList(res.data.data)
				}
				//localStorage.setItem("token",res.data.token)
			  })
		
	},[])
	
	const [show, setShow] = useState(false);
	const [name, setname] = useState("");
	const [description, setdescription] = useState("");

	const handleClose = () => setShow(false);
	const logout = (event) => {
		event.preventDefault()
		localStorage.removeItem('token')
		return navigate("/login");
	};
	const handleShow = (event,name,description) => {
	  setname(name)
	  setdescription(description)
	  event.preventDefault()
		  setShow(true)
	  };
	  
	  function validateForm() {
		return search.length > 0;
	  }

	  
	function handleSubmit(event) {
		event.preventDefault();
		if(search)
		{
			

			axios.post(`http://localhost:9999/user/search?search=${search}`,  "" )
				  .then(res => {
					console.log(res);
					console.log(res.data);
					if(res.data.success == true)
					{
						setList(res.data.data)
					}
					//localStorage.setItem("token",res.data.token)
				  })
			}
	
	}
  return (
    <div className="Login">
		<a href="" style={{float:'right'}} onClick={(event)=>logout(event)}>Logout</a>
	  <Form onSubmit={handleSubmit}>
        <Form.Group controlId="email">
        
          <Form.Control
            autoFocus
            type="text"
            value={search}
            onChange={(e) => setsearch(e.target.value)}
			placeholder="search"
          />
		 
        </Form.Group>
      
        
      </Form>
      <Table striped bordered hover>
		  <thead>
			<tr>
			  
			  <th>Name</th>
			  <th>Description</th>
			  <th>Action</th>
			</tr>
		  </thead>
		  <tbody>
		  {list.map((user, i) => (
                <>
                  <tr>
				  <td>{user.name}</td>
				  <td>{user.description}</td>
				  <td><a href="" onClick={(event)=>handleShow(event,user.name,user.description)}>click</a></td>
				</tr>
                </>
              ))}
		
			
		  </tbody>
		</Table>
		
		<Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{description}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          
        </Modal.Footer>
      </Modal>
    </div>
  );
}