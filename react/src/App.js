import logo from './logo.svg';
import './App.css';
import {  BrowserRouter,Route, Link, Routes , Redirect  } from 'react-router-dom'
import  Login  from './login'
import  List  from './list'
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Login/>} />
        <Route exact path="/login" element={<Login/>} />
        <Route exact path="/list" element={<List/>} />
       
      </Routes>
    </BrowserRouter>
  );
}

export default App;
