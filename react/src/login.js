import React, { useState,useEffect } from "react";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "./login.css";
import axios from 'axios';
import { useNavigate } from "react-router-dom";
export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  let navigate = useNavigate();
	useEffect(()=>{
		
		if(localStorage.getItem("token"))
		{
			 return navigate("/list");
		}
	},[])
  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
	if(email && password)
	{
		const user = {
		  email: email,
		  password: password
		};

		axios.post(`http://localhost:9999/user/login`,  user )
			  .then(res => {
				console.log(res);
				console.log(res.data);
				if(res.data.success == true)
				{
					localStorage.setItem("token",res.data.token)
					return navigate("/list");
				}
				//localStorage.setItem("token",res.data.token)
			  })
		}
	
  }

  return (
    <div className="Login">
      <Form onSubmit={handleSubmit}>
        <Form.Group size="lg" controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            autoFocus
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group size="lg" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        <Button block size="lg" type="submit" disabled={!validateForm()}>
          Login
        </Button>
      </Form>
    </div>
  );
}