const {  login,  searchList} = require("./user.controller");

const router = require("express").Router();

const { checkToken } = require("../auth/token_validation");

router.post("/login", login);

router.post("/search", searchList);

module.exports = router;