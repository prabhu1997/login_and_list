const pool = require("../../config/database");

module.exports = {
   
    getUserByUserName: (email, callBack) => {
        return new Promise((resolve) => {
            //setTimeout(() => {
            pool.query(
                'select * from user where name=?',
                email,
                (error, results, fields) => {

                    if (error) {
                        resolve(error);
                        callBack(error);
                    }
                    resolve(results[0]);
                    return callBack(null, results[0]);

                }

            );


            //}, 5000)
        });

    },

    search: (data, callBack) => {
        if (data) {
            var name = data;
        } else {
            var name = "";
        }
        pool.query(
            'select * from list where name like ?',
            [
                '%' + name + '%'
            ],
            (error, results, fields) => {
                if (error) {
                    return callBack(error);
                }
                return callBack(null, results);
            }
        );
    },

};