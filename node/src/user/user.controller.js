const { getUserByUserName, search } = require("./user.service");
const { tokenUser, baseURL, makeid } = require("../auth/common");

const { genSaltSync, hashSync, compareSync } = require("bcrypt");

const { sign } = require("jsonwebtoken");
const salt = genSaltSync(10);
module.exports = {
    
    login: (req, res) => {
        const body = req.body;
        if (body.email && body.password) {
            getUserByUserName(body.email, (err, results) => {
                if (err) {
                    console.log(err);
                }
                if (!results) {
                    return res.json({
                        success: false,
                        message: "Invalid email or password"
                    });
                }
                const result = compareSync(body.password, results.password);
                if (result) {
                    results.password = undefined;
                    const jsontoken = sign({ result: results }, process.env.AUTH_KEYVAL, {
                        expiresIn: "8h"
                    });
                    return res.json({
                        success: true,
                        message: "login successfully",
                        token: jsontoken,
                        userid: results.id
                    });
                }
                else {
                    return res.json({
                        success: false,
                        message: "Invalid email or password"
                    });
                }
            })
        }
        else {
            return res.json({
                success: false,
                message: "Invalid parameter"
            });
        }

    },

 
    searchList: async (req, res) => {
        var body=""
		if(req.query)
		{
			body = req.query.search
		}
		
        search(body, (err, results) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    success: false,
                    message: "Something went wrong"
                });
            }
            //console.log()
            return res.status(200).json({
                success: true,
                data: results
            });
        });


    },

}