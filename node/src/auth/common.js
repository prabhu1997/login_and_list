const { verify } = require("jsonwebtoken");

module.exports = {

    tokenUser: (token) => {
        return new Promise((resolve) => {
            //  let token = req.get("authorization");
            if (token) {
                //token = token.slice(7);
                verify(token, process.env.AUTH_KEYVAL, (err, decoded) => {
                    if (err) {
                        resolve();
                    }
                    else {
                        resolve(decoded.result)
                        //return decoded;
                    }
                })
            }
            else {
                resolve();
            }
        });
    },

    baseURL() {
        return "http://localhost:9999"
    },
    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }
};